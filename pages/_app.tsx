import '../styles/styles.css';
import type { AppProps } from 'next/app';
import { Header } from '../components/Header';
import { Navbar } from '../components/Navbar';
import { Container } from '../components/Container';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Container>
      <Header name="Lea's Gelabber" />
      <Navbar categories={['home', 'latest', 'esports']} />
      <Component {...pageProps} />
    </Container>
  );
}

export default MyApp;
