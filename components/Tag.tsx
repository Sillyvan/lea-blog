import Link from 'next/link';

type TagProps = { tag: string };

export const Tag: React.FC<TagProps> = ({ tag }) => (
  <div className='rounded-lg bg-slate-300 px-4 py-1 inline-block'>
    <Link href={`/tag/${tag}`} key={tag}>
      <a aria-label={`link to ${tag}`}>{tag}</a>
    </Link>
  </div>
);
