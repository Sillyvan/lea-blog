type CategoryContainerProps = {
  content?: React.ReactNode;
  sidebar?: React.ReactNode;
};

export const CategoryContainer: React.FC<CategoryContainerProps> = ({
  content,
  sidebar,
}) => (
  <div className='grid grid-cols-[4fr_1fr] gap-48'>
    <>{content}</>
    <>{sidebar}</>
  </div>
);
