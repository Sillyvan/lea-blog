import Link from 'next/link';
import { Line } from './Line';
import { Tag } from './Tag';

export type SidebarProps = {
  tags?: string[];
  filters?: string[];
};

export const Sidebar: React.FC<SidebarProps> = ({ tags, filters }) => {
  return (
    <div className=''>
      <input
        aria-label='search'
        type='search'
        className='border-2 border-black rounded-3xl px-2 mr-4 py-2 mb-4'
        placeholder='search'
      />
      <div>
        <h3 className='font-bold mb-2'>FILTER BY</h3>
        <div className='mb-4'>
          {filters?.map((filter) => (
            <div key={filter} className='flex gap-4'>
              <input type='checkbox' name='filters' />
              <p>{filter}</p>
            </div>
          ))}
        </div>
        <Line />
        <h3 className='my-4 font-bold'>WHAT MATTERS TO YOU</h3>
        <div className='flex gap-2'>
          {tags?.map((tag) => (
            <Tag tag={tag} key={tag} />
          ))}
        </div>
      </div>
    </div>
  );
};
