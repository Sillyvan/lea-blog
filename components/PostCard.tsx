import { Line } from './Line';
import { Tag } from './Tag';

export type PostCardProps = {
  title?: string;
  description?: string;
  image?: string;
  tags?: string[];
  readDuration?: string;
  date?: string;
};

export const PostCard: React.FC<PostCardProps> = ({
  title,
  description,
  image,
  tags,
  readDuration,
  date,
}) => (
  <>
    <div className='flex justify-center flex-col'>
      <div className='flex items-center gap-3 mb-2'>
        <h1 aria-label='post title' className='text-2xl font-semibold'>
          {title}
        </h1>
        <span className='text-xs'>{date}</span>
      </div>
      <p className='mb-2'>{description}</p>
      <div className='flex gap-4'>
        {tags?.map((tag) => (
          <Tag tag={tag} key={tag} />
        ))}
        <p>{readDuration} read</p>
      </div>
    </div>
    <div className='flex justify-end'>
      <img src={image} alt='image' />
    </div>
    <Line />
    <Line />
  </>
);
