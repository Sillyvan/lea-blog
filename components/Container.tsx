type ContainerProps = { children?: React.ReactNode };

export const Container: React.FC<ContainerProps> = ({ children }) => (
  <div className='flex flex-col items-center'>
    <div className='w-11/12'>{children}</div>
  </div>
);
