import Link from 'next/link';
import { useRouter } from 'next/router';
import { Line } from './Line';

export type NavbarProps = {
  categories?: string[];
};

export const Navbar: React.FC<NavbarProps> = ({ categories }) => {
  const router = useRouter();

  return (
    <div className='my-8'>
      <div className='my-4 flex w-full content-center gap-3'>
        {categories?.map((category) => (
          <Link href={`/category/${category}`} key={category}>
            <a
              aria-label={`link to ${category}`}
              className={
                router.asPath == `/category/${category}` ? 'text-pink-500' : ''
              }
            >
              <p className='text-xl'>{category}</p>
            </a>
          </Link>
        ))}
      </div>
      <Line />
    </div>
  );
};
